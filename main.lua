-- Ludum Dare 29: Beneath the Surface

-- classes
local Scene = require("Scene")
local Entity = require("Entity")
local Inputstate = require("Inputstate")

-- global
shared = {
  sound = {},
  fonts = {
    ptSansNarrow = "assets/pt-sans-narrow-regular.ttf"
  },
  images = {
    balloon = "assets/balloon.png",
    basketBoyLeft = "assets/basket-boy-left.png",
    basketBoyCenter = "assets/basket-boy-center.png",
    basketBoyRight = "assets/basket-boy-right.png",
    burner = "assets/burner.png",
    clouds = "assets/clouds.png",
    skyStars = "assets/sky-stars.png",
    skyClear = "assets/sky-clear.png",
    skyClouds = "assets/sky-clouds.png",
    tutorialKeyboard = "assets/tutorial-keyboard.png"
  }
}

-- data
local systems = require("systems")
local components = require("components")
local conf = require("conf")

-- internal
local accum = 0
local step = 1 / 120
local inputstate = nil -- init on load
local contexts = {
  story = {},
  simulation = {}
}
local currentContext = contexts.simulation


function love.load()
  math.randomseed(os.time())

  -- load shared
  for name, filename in pairs(shared.images) do
    shared.images[name] = love.graphics.newImage(filename)
  end
  for name, filename in pairs(shared.fonts) do
    shared.fonts[name] = love.graphics.newFont(filename, 24)
  end

  -- init
  inputstate = Inputstate.new()
  contexts.simulation.scene = Scene.new()
  contexts.story.scene = Scene.new()
  
  -- fill common parts
  for name, context in pairs(contexts) do
    context.inputstate = inputstate
    context.name = name
  end

  -- simulation context
  love.physics.setMeter(32)
  contexts.simulation.world = love.physics.newWorld(0, 9.8 * 32, true)

  local balloon = Entity.new("balloon", { components.box2d, components.image, components.script }, { "box2d-body", "layer-middle" })
  balloon.box2d.body = love.physics.newBody(contexts.simulation.world, 0, 0, "dynamic")
  balloon.box2d.shape = love.physics.newPolygonShape(0, -100, -70, -70, -100, 0, -62, 100, 0, 166, 62, 100, 100, 0, 70, -70)
  balloon.box2d.fixture = love.physics.newFixture(balloon.box2d.body, balloon.box2d.shape)
  balloon.image = shared.images.balloon
  balloon.imageCenter = {100, 100}

  local basket = Entity.new("basket", { components.box2d, components.image }, { "box2d-body", "layer-middle" })
  basket.box2d.body = love.physics.newBody(contexts.simulation.world, 0, 253, "dynamic")
  basket.box2d.shape = love.physics.newPolygonShape(16, -23, -57, 0, -51, 39, -5, 62, 56, 44, 66, -5)
  basket.box2d.fixture = love.physics.newFixture(basket.box2d.body, basket.box2d.shape)
  basket.image = shared.images.basketBoyCenter
  basket.imageCenter = {63, 43}

  local function fillRope(rope, sx, sy, ex, ey, distmod) -- TODO-FIX: find better name
    distmod = distmod or 1

    local dy = ey - sy
    local dx = ex - sx
    local dd = math.sqrt(dx * dx + dy * dy) * distmod

    local m1x, m1y = sx + dx / 4, sy + dy / 4
    local m2x, m2y = ex - dx / 4, ey - dy / 4

    local m1 = Entity.new(rope.name .. "-helper-m1", { components.box2d }, { "box2d-helper" }, rope1)
    m1.box2d.body = love.physics.newBody(contexts.simulation.world, m1x, m1y, "dynamic")
    m1.box2d.shape = love.physics.newCircleShape(2)
    m1.box2d.fixture = love.physics.newFixture(m1.box2d.body, m1.box2d.shape)

    local m2 = Entity.new(rope.name .. "-helper-m2", { components.box2d }, { "box2d-helper" }, rope1)
    m2.box2d.body = love.physics.newBody(contexts.simulation.world, m2x, m2y, "dynamic")
    m2.box2d.shape = love.physics.newCircleShape(2)
    m2.box2d.fixture = love.physics.newFixture(m2.box2d.body, m2.box2d.shape)

    rope.box2d.j1 = love.physics.newRopeJoint(balloon.box2d.body, m1.box2d.body, sx, sy, m1x, m1y, dd / 4, false)
    rope.box2d.j2 = love.physics.newRopeJoint(m1.box2d.body, m2.box2d.body, m1x, m1y, m2x, m2y, dd / 2, false)
    rope.box2d.j3 = love.physics.newRopeJoint(m2.box2d.body, basket.box2d.body, m2x, m2y, ex, ey, dd / 4, false)
    rope.box2d.m1 = m1
    rope.box2d.m2 = m2

    contexts.simulation.scene:addMany({ m1, m2 })
  end

  local rope1 = Entity.new("rope-1", { components.box2d }, { "box2d-balloon-rope", "layer-back" })
  fillRope(rope1, -48, 73, -56, 253)

  local rope2 = Entity.new("rope-2", { components.box2d }, { "box2d-balloon-rope", "layer-front" })
  fillRope(rope2, -12, 86, -12, 270)

  local rope3 = Entity.new("rope-3", { components.box2d }, { "box2d-balloon-rope", "layer-back" })
  fillRope(rope3, 65, 82, 62, 256)

  local rope4 = Entity.new("rope-4", { components.box2d }, { "box2d-balloon-rope", "layer-back" })
  fillRope(rope4, 10, 70, 17, 233)

  local ropeRing = Entity.new("rope-ring", { components.box2d }, { "layer-hidden" })

  local function makeRingSegment(body1, body2, tension)
    local x1, y1 = body1:getX(), body1:getY()
    local x2, y2 = body2:getX(), body2:getY()
    ropeRing.box2d.j4 = love.physics.newRopeJoint(body1, body2, x1, y1, x2, y2, tension)
    -- body
  end

  makeRingSegment(rope1.box2d.m1.box2d.body, rope2.box2d.m1.box2d.body, 80)
  makeRingSegment(rope2.box2d.m1.box2d.body, rope3.box2d.m1.box2d.body, 80)
  makeRingSegment(rope3.box2d.m1.box2d.body, rope4.box2d.m1.box2d.body, 80)
  makeRingSegment(rope4.box2d.m1.box2d.body, rope1.box2d.m1.box2d.body, 80)

  local burner = Entity.new("burner", { components.box2d, components.image }, { "box2d-body", "layer-middle"})
  burner.box2d.body = love.physics.newBody(contexts.simulation.world, 0, 203, "dynamic")
  burner.box2d.shape = love.physics.newRectangleShape(36, 29)
  burner.box2d.fixture = love.physics.newFixture(burner.box2d.body, burner.box2d.shape)
  burner.image = shared.images.burner
  burner.imageCenter = {18, 17}

  local ropeMiddle = Entity.new("rope-middle", { components.box2d }, { "box2d-middle-rope", "layer-hidden" })
  ropeMiddle.box2d.j1 = love.physics.newRopeJoint(balloon.box2d.body, burner.box2d.body, 0, 160, 0, 190, 30, false)
  ropeMiddle.box2d.j2 = love.physics.newRopeJoint(burner.box2d.body, basket.box2d.body, 0, 210, 0, 250, 40, false)

  local clouds = Entity.new("clouds", { components.position, components.image }, { "props", "layer-front" })
  clouds.position = {0, 1500}
  clouds.image = shared.images.clouds
  clouds.imageCenter = {480, 252}
  contexts.simulation.scene:add(clouds)

  local simulationCamera = Entity.new("camera",  { components.position }, { "layer-hidden" })
  simulationCamera.position = {0, 600}

  balloon.script = coroutine.create(function (self)
    while true do
      coroutine.yield()
      -- table.insert(self.tasklist, { tp = "approach-waypoint", waypoint = {100, 1000}})
      -- coroutine.yield()
    end
  end)

  contexts.simulation.scene:addMany({ balloon, basket, burner, rope1, rope2, rope3, rope4, ropeRing, ropeMiddle, simulationCamera})

  local function blink(target, time, delta)
    local visible = target.visible
    target.visible = true
    table.insert(target.tasklist, { tp = "wait", time = time / 2, delta = delta or 1 })
    coroutine.yield()
    table.insert(target.tasklist, { tp = "wait", time = time / 2, delta = -(delta or 1) })
    coroutine.yield()
    target.visible = visible
  end

  local function wait(target, time)
    local visible = target.visible
    target.visible = false
    table.insert(target.tasklist, { tp = "wait", time = time, delta = -1 })
    coroutine.yield()
    target.visible = visible
  end

  local storyboard = Entity.new("storyboard", { components.text, components.timer, components.script, components.visible })
  storyboard.font = shared.fonts.ptSansNarrow
  storyboard.script = coroutine.create(function (self)
    wait(self, 20)
    self.text = "I've been always wondering, what lies beneath the cloud surface..."
    blink(self, 5)
    self.text = "People were mad at me for asking this very question."
    blink(self, 5)
    self.text = "They say there's nothing there. Void. Endless emptiness."
    blink(self, 5)
    self.text = "They say it will collapse behind my back, once I dare to get trough it."
    blink(self, 5)
    self.text = "But I'm not scared. And I'm still curious."
    blink(self, 6)

    wait(self, 8)

    while not currentContext.done do
      self.text = "Should I do this?"
      blink(self, 3)
      if currentContext.done then break end
      wait(self, 2)
      if currentContext.done then break end
      self.text = "Will I be able to get back?"
      blink(self, 5)
      if currentContext.done then break end
      wait(self, 2)
      if currentContext.done then break end
      self.text = "Would I want to get back?"
      blink(self, 5)
      if currentContext.done then break end
      wait(self, 2)
    end

    wait(self, 2)
    self.text = "Well, I guess I'm still alive."
    blink(self, 4)
    self.text = "Oh, it's windy here!"
    blink(self, 4)

    wait(self, 2)
    self.text = "Wow, I see the color I'm not used to see!"
    blink(self, 4)
    self.text = "I think it's called... green. Right?"
    blink(self, 4)
    self.text = "It's rare to see something green above the clouds. Heh."
    blink(self, 4)
    wait(self, 2)

    self.text = "But there's a lot of blue too."
    blink(self, 4)
    self.text = "I guess it's common everywhere."
    blink(self, 4)

    wait(self, 2)
    self.text = "Wait, there's something else."
    blink(self, 3)
    self.text = "Let me get a bit closer."
    blink(self, 2)

    wait(self, 4)

    self.text = "I can't see details from here, but this may look like ..."
    blink(self, 6)
    self.text = "..."
    blink(self, 6)
    self.text = "... a village?"
    blink(self, 6)

    wait(self, 8)

    self.text = "simplesmiler\n\n\n\n\n\n\n\nLudum Dare 29: Beneath the clouds\n\n\n\n\n\n\n\n\n"
    blink(self, 16)
    
    self.text = "Press <q> to exit."
    blink(self, 16)
    
    self.text = "Or don't ;)"
    blink(self, 16)
    
    self.text = "Now you can press <q>. I promise you won't miss anything."
    blink(self, 16)
  end)

  local tutorialKeyboard = Entity.new("tutorial-keyboard", { components.position, components.image, components.timer, components.script, components.visible })
  tutorialKeyboard.image = shared.images.tutorialKeyboard
  tutorialKeyboard.imageCenter = {114, 74}
  tutorialKeyboard.script = coroutine.create(function (self)
    wait(self, 8)
    blink(self, 1)
    blink(self, 5)
  end)

  contexts.simulation.scene:addMany({ storyboard, tutorialKeyboard })
end

function love.update(dt)
  -- fix love2d derpiness
  local mx, my = love.mouse.getPosition()
  inputstate:mousemoved(mx, my)

  accum = accum + dt
  local updates = math.floor(accum / step)

  if updates > 5 then
    print("Skipped " .. tostring(updates) .. " updates")
    accum = accum - step * updates
  elseif updates > 0 then
    for i = 1, updates do
      systems.input.update(currentContext, step)
      systems.physics.update(currentContext, step)
      systems.logics.update(currentContext, step)
      accum = accum - step
    end

    inputstate:clearEvents()
  end
end

function love.draw()
  systems.graphics.update(currentContext, step, accum)
end


-- input binding

function love.keypressed(key, isrepeat)
  if key == "q" then
    love.event.quit()
  end
  inputstate:keypressed(key)
end

function love.keyreleased(key)
  inputstate:keyreleased(key)
end

function love.mousepressed(x, y, button)
  inputstate:mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
  inputstate:mousereleased(x, y, button)
end