local Inputstate = {}
local InputstateMethods = {}

function Inputstate.new()
  local inputstate = {
    keys = {},
    mpos = {0, 0},
    mbuttons = {},
    events = {}
  }

  return setmetatable(inputstate, { __index = InputstateMethods })
end

function InputstateMethods:clearEvents()
  self.events = {}
end

function InputstateMethods:addEvent(event)
  table.insert(self.events, event)
end

function InputstateMethods:keypressed(key)
  local last = self.keys[key]
  if not last then
    self.keys[key] = true
    self:addEvent({ source = "keypressed", key = key })
  end
end

function InputstateMethods:keyreleased(key)
  local last = self.keys[key]
  if last then
    self.keys[key] = nil
    self:addEvent({ source = "keyreleased", key = key })
  end
end

function InputstateMethods:mousemoved(x, y)
  local lx, ly = unpack(self.mpos)
  if x ~= lx or y ~= ly then
    self.mpos = {x, y}
    self:addEvent({ source = "mousemove", x = x, y = y })
  end
end

function InputstateMethods:mousescrolled(x, y, button)
  self:mousemoved(x, y)
  self:addEvent({ source = "mousescrolled", button = button, x = x, y = y })
end

function InputstateMethods:mousepressed(x, y, button)
  if button == "wu" or button == "wd" then 
    self:mousescrolled(x, y, button)
    return 
  end

  self:mousemoved(x, y)
  local last = self.mbuttons[button]
  if not last then
    self.mbuttons[button] = true
    self:addEvent({ source = "mousepressed", button = button, x = x, y = y })
  end
end

function InputstateMethods:mousereleased(x, y, button)
  self:mousemoved(x, y)
  local last = self.mbuttons[button]
  if last then
    self.mbuttons[button] = nil
    self:addEvent({ source = "mousereleased", button = button, x = x, y = y })
  end
end

return Inputstate