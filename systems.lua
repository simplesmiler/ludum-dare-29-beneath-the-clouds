local systems = {}

systems.physics = {
  update = function (context, step)
    if context.name == "simulation" then
      context.world:update(step)

      for name, entity in pairs(context.scene:queryTag("box2d")) do
        if entity:has("box2d-body") then
          entity.position = { entity.box2d.body:getX(), entity.box2d.body:getY() }
          entity.angle = entity.box2d.body:getAngle()
        end

        if entity.name == "balloon" then
          local vx, vy = entity.box2d.body:getLinearVelocity()

          -- air force
          entity.box2d.body:applyLinearImpulse(0, -60)

          -- limit speed
          if vy > 50 then
            entity.box2d.body:applyLinearImpulse(0, -120)
          end
          if -vy > 50 then
            entity.box2d.body:applyLinearImpulse(0, 20)
          end
          if vx > 30 then
            entity.box2d.body:applyLinearImpulse(-10, 0)
          end
          if -vx > 30 then
            entity.box2d.body:applyLinearImpulse(10, 0)
          end
        end
      end

      for name, entity in pairs(context.scene:queryTag("timer")) do
        if entity.timer ~= nil then
          entity.timer = entity.timer + step * entity.timerDelta
        end
      end

      local ballon = context.scene:getNamed("balloon")
      local camera = context.scene:getNamed("camera")
      local bx, by = ballon.box2d.body:getX(), ballon.box2d.body:getY()
      local cx, cy = unpack(camera.position)
      if by > 400 and cy < 1221 then
        context.controlled = true
        cy = cy + 0.25
      elseif by >= 1600 then
        context.done = true
        context.controlled = false
      end
      camera.position = {cx, cy}
    end -- if context.name == "simulation"
  end
}

systems.logics = {
  update = function (context, step)
    local entities = context.scene:queryTag("script")
    for name, entity in pairs(entities) do

      local ntasks = #entity.tasklist
      if ntasks == 0 then
        if entity.script ~= nil then
          coroutine.resume(entity.script, entity)
        end
      else
        local task = entity.tasklist[ntasks]

        if entity:has("box2d") then
          local task = entity.tasklist[#entity.tasklist]
          local x, y = entity.box2d.body:getX(), entity.box2d.body:getY()
          local vx, vy = entity.box2d.body:getLinearVelocity()

          if task.tp == "approach-waypoint" then
            if entity.name == "balloon" then
              local tx, ty = unpack(task.waypoint)
              local dx, dy = tx - x, ty - y
              local dd = math.sqrt(dx * dx + dy * dy)
              if dd < 5 then
                table.remove(entity.tasklist)
              else
                entity.box2d.body:applyLinearImpulse(dx / dd * 5, 0)
                if dy < 0 then
                  entity.box2d.body:applyLinearImpulse(0, math.max(dy, -1) * 120)
                end
              end
            else -- not balloon
              error(entity.name .. " doesn't know how to approach-waypoint")
            end
          end -- approach-waypoint

          if task.tp == "fly-steady" then
            if entity.name == "balloon" then
              local dirx, diry = unpack(task.direction)
              entity.box2d.body:applyLinearImpulse(dirx * 5, 0)
              if diry < 0 then
                entity.box2d.body:applyLinearImpulse(0, diry * 120)
              end
            else
              error(entity.name .. " doesn't know how to fly-steady")
            end
          end
        end -- box2d

        if entity:has("timer") then
          if task.tp == "wait" then
            if entity.timer == nil then
              if task.delta < 0 then
                entity.timer = task.time
              else
                entity.timer = 0
              end
              entity.timerDelta = task.delta
            elseif (task.delta < 0 and entity.timer <= 0) or (task.delta > 0 and entity.timer >= task.time) then
              entity.timer = nil
              table.remove(entity.tasklist)
              coroutine.resume(entity.script, entity)
            end
          end
        end
      end
    end
  end
}

systems.input = {
  update = function (context, step)
    local inputstate = context.inputstate

    if context.name == "simulation" then
      local balloon = context.scene:getNamed("balloon")
      local basket = context.scene:getNamed("basket")
      local by = balloon.box2d.body:getY()
      if by > -50 then
        -- balloon.box2d.body:applyLinearImpulse(0, -120)
      end

      if context.controlled then
        if inputstate.keys["w"] or inputstate.keys["up"] then
          balloon.box2d.body:applyLinearImpulse(0, -100)
          basket.image = shared.images.basketBoyCenter
        end

        if inputstate.keys["a"] or inputstate.keys["left"] then
          balloon.box2d.body:applyLinearImpulse(-10, 10)
          basket.image = shared.images.basketBoyLeft
        elseif inputstate.keys["d"] or inputstate.keys["right"] then
          balloon.box2d.body:applyLinearImpulse(10, 10)
          basket.image = shared.images.basketBoyRight
        end
      end
    end -- if context.name == "simulation"
  end
}

systems.graphics = {
  update = function (context, step, accum)
    love.graphics.translate(480, 270)

    if context.name == "simulation" then
      -- TODO: draw weather correctly

      local camera = context.scene:getNamed("camera")
      local cx, cy = unpack(camera.position)

      systems.graphics.draw_sky_(cy)

      love.graphics.translate(-cx, -cy)

      systems.graphics.update_layer_(context, step, accum, "layer-back")
      systems.graphics.update_layer_(context, step, accum, "layer-middle")
      systems.graphics.update_layer_(context, step, accum, "layer-front")

      -- for name, entity in pairs(context.scene:queryTag("props")) do
      --   if entity:has("image") and entity:has("position") then
      --     local ox, oy = unpack(entity.imageCenter)
      --     local x, y = unpack(entity.position)
      --     love.graphics.setColor(255, 255, 255, 255)
      --     love.graphics.draw(entity.image, x, y, entity.angle, 1, 1, ox, oy)
      --   end
      -- end

      love.graphics.translate(cx, cy)

      local storyboard = context.scene:getNamed("storyboard")
      local width, lines = storyboard.font:getWrap(storyboard.text, 960)
      local fheight = storyboard.font:getHeight()

      local time = math.max(storyboard.timer or 0, 0)
      local opacity = math.min(0.5, time) * 2 * 255
      if not storyboard.visible then
        opacity = 0
      end
      love.graphics.setFont(storyboard.font)
      love.graphics.setColor(255, 255, 255, opacity)
      love.graphics.printf(storyboard.text, -480, - lines * fheight / 2, 960, "center")

      local tutorialKeyboard = context.scene:getNamed("tutorial-keyboard")
      local tut_time = math.max(tutorialKeyboard.timer or 0, 0)
      local tut_ox, tut_oy = unpack(tutorialKeyboard.imageCenter)
      local tut_opacity = math.min(0.5, tut_time) * 255
      if not tutorialKeyboard.visible then
        tut_opacity = 0
      end
      love.graphics.setColor(255, 255, 255, tut_opacity)
      love.graphics.draw(tutorialKeyboard.image, 480, 270, 0, 1, 1, tut_ox + 16, tut_oy + 16)

      love.graphics.translate(-480, -270)

    end -- if context.name == "simulation"
  end,

  draw_sky_ = function (camera_y)
    love.graphics.setColor(255, 255, 255, 255)
    if camera_y <= 700 then
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.draw(shared.images.skyStars, -480, -270)
    elseif camera_y > 700 and camera_y <= 1000 then
      local opacity = (camera_y - 700) / 300 * 255
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.draw(shared.images.skyStars, -480, -270)
      love.graphics.setColor(255, 255, 255, opacity)
      love.graphics.draw(shared.images.skyClear, -480, -270)
    elseif camera_y > 1000 and camera_y <= 1100 then
      local opacity = math.min(1, (camera_y - 1000) / 100) * 255
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.draw(shared.images.skyClear, -480, -270)
      love.graphics.setColor(255, 255, 255, opacity)
      love.graphics.draw(shared.images.skyClouds, -480, -270)
    elseif camera_y > 1100 then
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.draw(shared.images.skyClouds, -480, -270)
    end
  end,

  update_layer_ = function (context, step, accum, layertag)
    for name, entity in pairs(context.scene:queryTagAll({ "box2d", layertag })) do
      if entity:has("box2d-body") then
        local x, y = unpack(entity.position)
        if entity:has("image") then
          local ox, oy = unpack(entity.imageCenter)
          love.graphics.setColor(255, 255, 255, 255)
          love.graphics.draw(entity.image, x, y, entity.angle, 1, 1, ox, oy)
        end
      elseif entity:has("box2d-balloon-rope") then
        local sx, sy, m1x, m1y = entity.box2d.j1:getAnchors()
        local m2x, m2y, ex, ey = entity.box2d.j3:getAnchors()
        local bezier = love.math.newBezierCurve(sx, sy, m1x, m1y, m2x, m2y, ex, ey)
        love.graphics.setColor(16, 16, 16, 255)
        love.graphics.setLineWidth(2)
        love.graphics.line(bezier:render(2))
      end
    end
    for name, entity in pairs(context.scene:queryTagAll({ "props", layertag })) do
      local x, y = unpack(entity.position)

      if entity:has("image") then
        local ox, oy = unpack(entity.imageCenter)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.draw(entity.image, x, y, entity.angle, 1, 1, ox, oy)
      end
    end
  end
}

return systems