local Scene = {}
local SceneMethods = {}

function Scene.new(all)
  local scene = {
    all = all or {}
  }

  return setmetatable(scene, { __index = SceneMethods })
end

function SceneMethods:add(entity)
  assert(entity)
  
  local name = entity.name
  self.all[name] = entity
end

function SceneMethods:addMany(entlist)
  for _, entity in ipairs(entlist) do
    local name = entity.name
    self.all[name] = entity
  end
end

function SceneMethods:query(filter)
  assert(filter)
  local filtered = {}

  for name, entity in pairs(self.all) do
    if filter(entity) then filtered[name] = entity end
  end

  return filtered
end

function SceneMethods:getNamed(name)
  assert(name)
  local entity = self.all[name]
  assert(entity, "No entity named " .. name)

  return entity
end

function SceneMethods:queryTag(tagname)
  return self:query(function (entity)
    return entity:has(tagname)
  end)
end

function SceneMethods:queryTagAll(tags)
  return self:query(function (entity)
    for _, tag in ipairs(tags) do
      if not entity:has(tag) then return false end
    end
    return true
  end)
end

function SceneMethods:queryTagAny(tags)
  return self:query(function (entity)
    for _, tag in ipairs(tags) do
      if entity:has(tag) then return true end
    end
    return false
  end)
end



return Scene