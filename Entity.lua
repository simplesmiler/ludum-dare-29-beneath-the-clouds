local Entity = {}
local EntityMethods = {}

function Entity.new(name, cons, extratags, parent)
  assert(name)
  cons = cons or {}
  extratags = extratags or {}

  local entity = {
    name = name,
    tags = {},
    parent = nil
  }

  for _, con in ipairs(cons) do
    con(entity)
  end

  for _, tag in ipairs(extratags) do
    entity.tags[tag] = true
  end

  return setmetatable(entity, { __index = EntityMethods })
end

function EntityMethods:has(tag)
  return self.tags[tag]
end

function EntityMethods:hasAll(tags)
  for _, tag in ipairs(tags) do
    if not self.tags[tag] then return false end
  end
  return true
end

function EntityMethods:hasAny(tags)
  for _, tag in ipairs(tags) do
    if self.tags[tag] then return true end
  end
  return false
end

return Entity