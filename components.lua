local components = {}

function components.position(entity)
  entity.position = {0, 0}
  entity.angle = 0

  entity.tags.position = true
end

function components.box2d(entity)
  components.position(entity)
  
  entity.box2d = {}
  entity.tags.box2d = true
end

function components.image(entity)
  entity.image = nil
  entity.imageCenter = {0, 0}
  entity.tags.image = true
end

function components.script(entity)
  entity.tasklist = {}
  entity.script = nil
  entity.tags.tasklist = true
  entity.tags.script = true
end

function components.text(entity)
  entity.text = ""
  entity.tags.text = true
end

function components.timer(entity)
  entity.timer = nil
  entity.timerDelta = -1
  entity.tags.timer = true
end

function components.visible(entity)
  entity.visible = true
  entity.tags.visible = true
end

return components