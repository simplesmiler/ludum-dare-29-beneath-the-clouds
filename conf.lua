local conf = {
  title = "Ludum Dare 29: Beneath the clouds",
  width = 960,
  height = 540
}

function love.conf(t)
    t.version = "0.9.0"

    t.window.title = conf.title        -- The window title (string)
    t.window.width = conf.width        -- The window width (number)
    t.window.height = conf.height      -- The window height (number)
    t.window.resizable = false         -- Let the window be user-resizable (boolean)
    t.window.fullscreen = false        -- Enable fullscreen (boolean)
    t.window.vsync = true              -- Enable vertical sync (boolean)
end

return conf